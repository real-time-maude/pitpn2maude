// TPN name=/home/arias/Work/code/esprits/pitpn2maude/benchmarks/models/tutorial.cts
// insert here your type definitions using C-like syntax


// insert here your function definitions
// using C-like syntax






initially {
 // insert here the state variables declarations
// and possibly some code to initialize them
// using C-like syntax


int  start=1, childStart=0, fatherCont=0, childDone=0, fatherDone=0, joined=0; }

 transition [ intermediate {   start =  start  - 1; }]  fork [LI,LS]
      when (start >= 1)
      {   start = start  - 1 , childStart = childStart + 1 , fatherCont = fatherCont + 1; }
 transition [ intermediate {   childStart =  childStart  - 1; }]  childExec [30,50]
      when (childStart >= 1)
      {   childStart = childStart  - 1 , childDone = childDone + 1; }
 transition [ intermediate {   fatherCont =  fatherCont  - 1; }]  fatherExec [30,40]
      when (fatherCont >= 1)
      {   fatherCont = fatherCont  - 1 , fatherDone = fatherDone + 1; }
 transition [ intermediate {   fatherDone =  fatherDone  - 1 , childDone =  childDone  - 1; }]  join [0,0]
      when (childDone >= 1 and fatherDone >= 1)
      {   fatherDone = fatherDone  - 1 , childDone = childDone  - 1 , joined = joined + 1; }
 transition [ intermediate {   joined =  joined  - 1; }]  startOver [0,0]
      when (joined >= 1)
      {   joined = joined  - 1 , start = start + 2; }


  // insert TCTL formula here : check formula
  check [ timed_trace, restrict] EF ( <replace> )
