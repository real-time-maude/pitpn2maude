// TPN name=/home/arias/.romeo/temp/ctsfile.cts
// insert here your type definitions using C-like syntax

// insert here your function definitions
// using C-like syntax





 
 

initially { 

// insert here the state variables declarations
// and possibly some code to initialize them
// using C-like syntax

  

int  itemReady=0, buffer=0, itemReceived=0, readyConsumer=1, readyProducer=1; }

 transition [ intermediate {   readyProducer =  readyProducer  - 1; }]  produce [2,6]
      when (readyProducer >= 1)
      { 
        readyProducer = readyProducer  - 1 , itemReady = itemReady + 1; }
 transition [ intermediate {   itemReady =  itemReady  - 1; }]  send [2,4]
      when (itemReady >= 1)
      { 
        itemReady = itemReady  - 1 , readyProducer = readyProducer + 1 , buffer = buffer + 1; }
 transition [ intermediate {   buffer =  buffer  - 1 , readyConsumer =  readyConsumer  - 1; }]  receive [a,a]
      when (readyConsumer >= 1 and buffer >= 1)
      { 
        buffer = buffer  - 1 , readyConsumer = readyConsumer  - 1 , itemReceived = itemReceived + 1; }
 transition [ intermediate {   itemReceived =  itemReceived  - 1; }]  consume [0,0]
      when (itemReceived >= 1)
      { 
        itemReceived = itemReceived  - 1 , readyConsumer = readyConsumer + 1; }


 check [ timed_trace, restrict] AG ((  itemReady<=1 and buffer<=1 and itemReceived<=1 and readyConsumer<=1 and readyProducer<=1))
