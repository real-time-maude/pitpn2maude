// TPN name=/home/arias/.romeo/temp/ctsfile.cts
// insert here your type definitions using C-like syntax

// insert here your function definitions
// using C-like syntax





 parameters  a > 48 
 

initially { 

// insert here the state variables declarations
// and possibly some code to initialize them
// using C-like syntax

  

int  ready1=1, ready2=1, ready3=1, ending1=0, ending2=0, ending3=0; }

 transition act1 [a,a]
      when (true)
      { 
        ready1 = ready1 + 1; }
 transition act2 [a*2,a*2]
      when (true)
      { 
        ready2 = ready2 + 1; }
 transition act3 [a*3,a*3]
      when (true)
      { 
        ready3 = ready3 + 1; }
 transition [ intermediate {   ending1 =  ending1  - 1; }]  end1 [0,0]
      when (ending1 >= 1)
      { 
        ending1 = ending1  - 1; }
 transition [ intermediate {   ready1 =  ready1  - 1; }]  exec1 [10,20]
      when (ready1 >= 1)
      { 
        ready1 = ready1  - 1 , ending1 = ending1 + 1; }
 transition [ intermediate {   ready2 =  ready2  - 1; }]  exec2 [18,28]
      when (ready2 >= 1 and ready1 < 1)
      { 
        ready1 = ready1  , ready2 = ready2  - 1 , ending2 = ending2 + 1; }
 transition [ intermediate {   ending2 =  ending2  - 1; }]  end2 [0,0]
      when (ending2 >= 1)
      { 
        ending2 = ending2  - 1; }
 transition [ intermediate {   ending3 =  ending3  - 1; }]  end3 [0,0]
      when (ending3 >= 1)
      { 
        ending3 = ending3  - 1; }
 transition [ intermediate {   ready3 =  ready3  - 1; }]  exec3 [20,28]
      when (ready1 < 1 and ready2 < 1 and ready3 >= 1)
      { 
        ready3 = ready3  - 1 , ready2 = ready2  , ready1 = ready1  , ending3 = ending3 + 1; }


 check [ timed_trace, restrict] AG ((  ready1<=1 and ready2<=1 and ready3<=1 and ending1<=1 and ending2<=1 and ending3<=1))
