#!/bin/bash

# Romeo binary
romeo="/opt/romeo-3.8.6/bin/romeo-cli"

# maude binary
declare -A maude_solvers=(
  ['yices']="/opt/maude-se/maude-se-yices2/maude-se-yices2"
  ['z3']="/opt/maude-se/maude-se-z3-tactic-linux/maude-se-z3-tactic.linux64"
)

# pitpn2maude parser
BASE_DIR="$(dirname "$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)")"
parser="${BASE_DIR}/pitpn2maude/app.py"

# maude theory files
maude_theories="${BASE_DIR}/maude"

# folder containing the folder
model_folder="models"

# theories
theories=("symbolic-theory" "symbolic-theory2" "symbolic-folding-tree")

# run parser pitpn2maude
function parser_files {
  local model=$1
  local theory=$2
  echo "parsing" $model "with theory:" "$theory" "..."
  python3 $parser --input "${model_folder}/${model}.xml" --output "${model_folder}/${model}.${theory}.maude" --theory "${theory}"
}

# run romeo
function run_romeo {
  local filename=$1
  local timeout_value=$2
  timeout $timeout_value $romeo -m "${filename}" || echo "timeout ${timeout}"
}

# run Maude
function run_maude {
  local filename=$1
  local timeout_value=$2
  local version=$3
  timeout "$timeout_value" "${maude_solvers[$version]}" -no-banner -batch "${filename}" || echo "timeout ${timeout}"
}

# create a folder
function create_folder {
  local path=$1

  if [ ! -d "$path" ]; then
    mkdir -p "$path"
  fi
}

# Print a help message.
function usage() {
  echo "Usage: $0 -m MODEL -t TIMEOUT [-n N_TOKENS] [-s]" 1>&2
}

# Exit with error.
exit_abnormal() {
  usage
  exit 1
}

# Run benchmarks
function run_benchmark {
  local model=$1
  local tokens=$2
  local timeout=$3

  # create folder to save the output of the model
  output_folder="logs/${model}"
  create_folder "${output_folder}"
  cp "${maude_theories}/"*.maude "${output_folder}"

  # 1) generate the full model with parser
  for theory in ${theories[@]}; do
    if [ "$theory" != "symbolic-folding-tree" ]; then
      parser_files $model "$theory"
    fi
  done

  # 2) get the places of the model
  places=$(cat ${model_folder}/${model}.xml | sed -E -n '/<place/s/^.*[ ]label="([^"]+)".*$/\1/p' | awk '{ print $NF }')

  for p in $places; do
    # 3) run romeo
    romeo_file="${model}.cts"
    new_romeo_file="${output_folder}/${romeo_file}.${p}"

    if [[ ! -f "${new_romeo_file}" ]]; then
      property="${p}>${tokens}"
      sed "s/<replace>/${property}/" "${model_folder}/${romeo_file}" >"${new_romeo_file}"

      echo "Running Romeo with ${new_romeo_file}"
      (time run_romeo "${new_romeo_file}" "${timeout}") >"${new_romeo_file}.res" 2>&1
    fi

    # 4) run maude
    for theory in ${theories[@]}; do
      # maude files
      maude_file="${model}.${theory}.maude"

      # skip if file does not exist
      if [ ! -f "${model_folder}/$maude_file" ]; then
        echo "⚠" $maude_file "does not exist"
        continue
      fi

      for solver in "${!maude_solvers[@]}"; do
        # skip solvers when theory is folding-tree
        if [ "$theory" = "symbolic-folding-tree" ] && [ "$solver" != "z3" ]; then
          continue
        fi

        new_maude_file="${output_folder}/${maude_file}_${solver}.${p}"

        if [[ ! -f "${new_maude_file}" ]]; then
          placeName="p${p^}"

          if [ "$theory" = "symbolic-folding-tree" ]; then
            place="$placeName"
            marking=$((tokens + 1))
          else
            place="(${placeName} |-> D:Integer)"
            marking="D:Integer > ${tokens}"
          fi

          sed "s/<replace_p>/${place}/" "${model_folder}/${maude_file}" >"${new_maude_file}"
          sed -i "s/<replace_m>/${marking}/" "${new_maude_file}"

          echo "Running Maude (${solver} solver) with ${new_maude_file}"
          run_maude "${new_maude_file}" "${timeout}" $solver >"${new_maude_file}.res"
        fi
      done
    done
  done
}

# Run 1-safe benchmarks
function run_safety_benchmark {
  local model=$1
  local timeout=$2

  # create folder to save the output of the model
  output_folder="logs/${model}"
  create_folder "${output_folder}"
  cp "${maude_theories}/"*.maude "${output_folder}"

  # 3) run romeo
  romeo_file="${model}.1safe.cts"
  new_romeo_file="${output_folder}/${romeo_file}"

  if [[ ! -f "${new_romeo_file}" ]]; then
    cp "${model_folder}/$romeo_file" "$new_romeo_file"

    echo "Running Romeo with ${new_romeo_file}"
    (time run_romeo "${new_romeo_file}" "${timeout}") >"${new_romeo_file}.res" 2>&1
  fi

  # 4) run maude
  solver="z3"
  maude_file="${model}.1safe.maude"
  new_maude_file="${output_folder}/${maude_file}_${solver}"

  if [[ ! -f "${new_maude_file}" ]]; then
    cp "${model_folder}/$maude_file" "$new_maude_file"

    echo "Running Maude (${solver} solver) with ${new_maude_file}"
    run_maude "${new_maude_file}" "${timeout}" $solver >"${new_maude_file}.res"
  fi
}

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------

TIMEOUT=""
MODEL=""
TOKENS="2"
SAFETY="false"

while [[ "$#" -gt 0 ]]; do
  case "$1" in
  -m | --model)
    MODEL="$2"
    shift
    ;;
  -t | --timeout)
    TIMEOUT="$2"
    shift
    ;;
  -n | --tokens)
    TOKENS="$2"
    shift
    ;;
  -s | --safety)
    SAFETY="true"
    ;;
  *) exit_abnormal ;;
  esac
  shift
done

if [ "$MODEL" = "" ] || [ "$TIMEOUT" = "" ]; then
  exit_abnormal
fi

# use "all" for run all the experiments
if [ "$MODEL" = "all" ]; then
  MODEL=$(find "$model_folder" -name "*.cts" -type f -exec basename {} \;)
fi

# run benchmark
for m in $(echo "$MODEL"); do
  if [ "$SAFETY" = "false" ]; then
    run_benchmark "${m%%.*}" "${TOKENS}" "${TIMEOUT}"
  else
    run_safety_benchmark "${m%%.*}" "${TIMEOUT}"
  fi
done
