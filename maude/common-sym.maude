***(
-------------
common-sym.maude
-------------

General definitions common to all the symbolic theories defining the semantics
of timed Petri nets. 

Modules
-------

DATA-TYPES:
 - Definition of labels (names for transitions) and places (using Maude
   strings). 

PETRI-NET-REPRESENTATION:
 - Representation of markings as mapping from places to Integer (SMT sort)
 - Representation of clocks/firing times as maps from labels (transitions) to
   Rational number (SMT sort)
 - Representation of nets as a set of transition. Each transition uses a label,
   PRE, POST and INHIBITED markings and a time interval.
 - Representation of intervals including "inf" as an upper bound. 
 - Operations on markings: +, - , <=
 - Common definitions for the semantics of nets: enabled, inhibited, etc. 

*)

load smt
load smt-check

--- Labels (transitions) and places. 
fmod DATA-TYPES is
  protecting STRING .

  sorts Label Place . 
  subsort String < Place .    
  subsort String < Label .    

  --- Some dummy places and transition to ease writing examples 
  ops p0 p1 p2 p3 p4 p5 p6 p7 p8 p9 : -> Place [ctor] .
  ops t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 : -> Label [ctor] .

endfm

--- Markings, time-intervals and mappings from labels to Rat representing
--- clocks in transitions

fmod PETRI-NET-REPRESENTATION is 
   protecting DATA-TYPES .
   protecting SMT-CHECK .
   protecting REAL-EXPR .
   protecting INTEGER-EXPR .

   --- ----------------------------------------------- 
   vars T T1 T2 T3                   : Time .
   var TI                            : TimeInf .
   vars N N1 N2                      : IntegerExpr .
   vars P P1 P2                      : Place .
   vars M M1 M2 PRE POST INHIBIT     : Marking .
   var NET                           : Net .
   var TR                            : Transition .
   var INTERVAL                      : Interval .
   var L                             : Label .
   vars R1 R2                        : RealExpr .
   var FT                            : FiringTimes .
   var S                             : String .
   var ID-NAT                        : Nat-Now .
   var C                             : BooleanExpr .          
   --- ----------------------------------------------- 

   --- time
   sorts Time TimeInf .  
   op inf : -> TimeInf [ctor] .
   subsort RealExpr < Time < TimeInf . 

   --- transition and nets 
   sorts Net Transition .    
   subsort Transition < Net .
   op tr : Label Marking Marking Marking Interval -> Transition [ctor] .
   op emptyNet : -> Net [ctor] .
   op _;_ : Net Net -> Net [ctor assoc comm id: emptyNet format (n r o o) prec 65 ] .

   --- markings
   sort Marking .
   op empty : -> Marking [ctor] .
   op _|->_ : Place IntegerExpr -> Marking [ctor prec 55 format(b o b o)] .
   op _;_ : Marking Marking -> Marking [ctor assoc comm id: empty prec 65 ] .

   --- clocks 
   sort FiringTimes .
   op empty : -> FiringTimes [ctor] .
   op _->_ : Label Time -> FiringTimes [ctor prec 55 format(m o m o)] .
   op _;_ : FiringTimes FiringTimes -> FiringTimes [ctor assoc comm id: empty prec 65] .

   --- intervals 
   sort Interval .
   op `[_:_`] : Time TimeInf -> Interval [ctor format (o g o g o o)] .

   --- Operations on markings:
   op _+_ : Marking Marking -> Marking .
   --- assume that the first argument contains ALL places
   eq ((P |-> N1) ; M1) + ((P |-> N2) ; M2) = (P |-> N1 + N2) ; (M1 + M2) .
   eq M1 + empty = M1 .

   op _-_ : Marking Marking -> Marking .
   --- assume that the first argument contains ALL places
   eq ((P |-> N1) ; M1) - ((P |-> N2) ; M2) = (P |-> N1 - N2) ; (M1 - M2) .
   eq M1 - empty = M1 .


   op _<=_ : Marking Marking -> BooleanExpr .
   --- assume as always that second argument contains all places
   eq ((P |-> N1) ; M1) <= ((P |-> N2) ; M2) = N1 <= N2 and (M1 <= M2) .
   eq empty <= M2 = true .

   --- Enabled (no time interval considered here)
   op enabled : Marking Transition -> BooleanExpr .
   eq enabled(M, tr(L, PRE, POST, INHIBIT, INTERVAL)) =
         (PRE <= M) and not inhibited(M, INHIBIT) .

   op inhibited : Marking Transition -> BooleanExpr .
   op inhibited : Marking Marking -> BooleanExpr .   --- state, inhibited
   eq inhibited(M, empty) = false .
   eq inhibited((P |-> N2) ; M, (P |-> N) ; INHIBIT) =
         ((N > 0) and (N2 >= N)) or inhibited(M, INHIBIT) .

   eq inhibited(M, tr(L, PRE, POST, INHIBIT, INTERVAL)) =
         inhibited(M, INHIBIT) .

   --- Simplification of SMT expressions
   op simplifyFormula : Marking -> Marking .
   eq simplifyFormula (( P |-> N1) ; M1) = (P |-> simplifyFormula(N1)) ; simplifyFormula(M1) .
   eq simplifyFormula((empty).Marking) = (empty).Marking .

   op simplifyFormula : FiringTimes -> FiringTimes .
   eq simplifyFormula (( L -> R1) ; FT) = ((L -> simplifyFormula(R1)) ; simplifyFormula(FT)) .
   eq simplifyFormula((empty).FiringTimes) = (empty).FiringTimes .

   --- Fresh variables 
   --- IDs for variables representing parameters, markings, clocks and tick values
   sorts ParID PlaceID ClockID TimeID . 
   subsort ParID PlaceID ClockID TimeID < SMTVarId .
   sort Nat-Now .

   --- now is used for the folding procedure (the variables for the current step)
   subsort Nat < Nat-Now .
   op now : -> Nat-Now [ctor] .

   op p : String -> ParID .
   op m : Nat-Now Place -> PlaceID .
   op c : Nat-Now Label -> ClockID .  
   op t : Nat-Now  -> TimeID  .
   op gt : Nat-Now  -> TimeID  .

   --- Operators for building the needed Real/Integer expression for variables
   op vp : String -> RealVar .
   eq vp(S) = r(p(S)) .

   op vm : Nat-Now Place -> IntegerVar .
   eq vm(ID-NAT, P) = i(m(ID-NAT, P)) .

   op vc : Nat-Now Label -> RealVar .
   eq vc (ID-NAT, L) = r(c(ID-NAT, L)) .

   op vt : Nat-Now -> RealVar .
   eq vt(ID-NAT) = r(t(ID-NAT)) .

   --- Global clock
   op vgt : Nat-Now -> RealVar .
   eq vgt(ID-NAT) = r(gt(ID-NAT)) .

   --- SMT Operations 
   op min : RealExpr RealExpr -> RealExpr .
   eq min(R1,R2) = R1 <= R2 ? R1 : R2 .

   op min : TimeInf TimeInf -> TimeInf [ditto] .
   eq min(TI, inf) = TI .

   --- Conditions for transitions 
   op _in_ : Time Interval -> BooleanExpr .
   eq T in [T1 : T2] = (T1 <= T) and (T <= T2) .
   eq T in [T1 : inf] = T1 <= T .

   --- A firing time where all clocks are zero
   op zero-clock : Net -> FiringTimes .
   eq zero-clock(emptyNet) = empty .
   eq zero-clock( tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET ) = 
      L -> 0/1 ; zero-clock(NET) .

  --- checking for validity of SMT formulas
  op validity : BooleanExpr -> Bool .
  eq validity(C) = smtCheck(C) == false .

  --- k-bounded Marking 
  op bounded-k : Integer Marking -> Marking .
  eq bounded-k(N, empty) = empty .
  eq bounded-k(N,  (P |-> N1) ; M) = (P |-> N) ; bounded-k(N, M) .

 --- Checking if a net is alive
 op alive : Net Marking FiringTimes BooleanExpr -> Bool .
 op alive : Net Marking FiringTimes -> BooleanExpr .
 eq alive(emptyNet, M, FT) = false .
 eq alive(tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET, M, ((L -> R1) ; FT)) = 
          ((PRE <= M) and (R1 in INTERVAL) and
          not inhibited(M, tr(L, PRE, POST, INHIBIT, INTERVAL))) or
          alive(NET , M, FT) .
 eq alive(NET, M, FT, C) = smtCheck(C and alive(NET, M, FT)) .
endfm
