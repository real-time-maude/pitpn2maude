***(
-------------
concrete-theory3.maude
-------------
Similar to ./concrete-theory1.maude but:
- A global clock is added to the state

*)

load common .

mod PETRI-NET-DYNAMICS is

   protecting PETRI-NET-REPRESENTATION .

   --- State:
   sort State .
   op _:_:_:_ : Time Marking FiringTimes Net -> State [ctor format (n d n d n d n d) prec 75] .

   --- ---------------------------------------------------------------
   vars T T1 T2 T3                                     : Time        .
   var  GT STEP                                        : Time        .
   var TI                                              : TimeInf     .
   var NET                                             : Net         .
   vars M PRE POST INHIBIT                             : Marking     .
   vars PRE-M INTERMEDIATE-M POST-M                    : Marking     .
   var L                                               : Label       .
   var FT                                              : FiringTimes .
   var INTERVAL                                        : Interval    .
   --- ---------------------------------------------------------------
   
   --- Marking is updated according to the transition
   --- The clock for the transition L is reset 
   crl [applyTransition] :
       GT : M : ((L -> T) ; FT) : (tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET)
     =>
       GT : ((M - PRE) + POST) :
       ((L -> 0) ; updateFiringTimes(FT, (M - PRE), (M - PRE) + POST, NET)) :
       (tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET)
    if (PRE <= M) and (T in INTERVAL) and
        not inhibited(M, tr(L, PRE, POST, INHIBIT, INTERVAL)) .

  --- Non-executable version
  crl [tick] :
      GT : M : FT : NET
     =>
      GT + T : M : addFiringTimes(M, FT, NET, T) : NET
    if  T <= mte(M, FT, NET) [nonexec] .

  --- Executable (incomplete) tick incrementing by 1
  crl [executableTick] :
      GT : M : FT : NET
     =>
      GT + STEP : M : addFiringTimes(M, FT, NET, STEP) : NET
    if  STEP := 1 /\
        STEP <= mte(M, FT, NET) .

   --- Updating clocks by checking if the transition is newly enabled. 
   op updateFiringTimes : FiringTimes Marking Marking Net -> FiringTimes .
   eq updateFiringTimes(empty, INTERMEDIATE-M, POST-M, NET) = empty .
   eq updateFiringTimes((L -> T) ; FT, INTERMEDIATE-M, POST-M,
                        tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET) =
      if (not enabled(INTERMEDIATE-M, tr(L, PRE, POST, INHIBIT, INTERVAL)))
          and enabled(POST-M, tr(L, PRE, POST, INHIBIT, INTERVAL))
        then (L -> 0) 
        else (L -> T) 
      fi ; updateFiringTimes(FT, INTERMEDIATE-M, POST-M, NET) .

  --- Max value for time advance. 
  op mte : Marking FiringTimes Net -> TimeInf .
  eq mte(M, (L -> T) ; FT, tr(L, PRE, POST, INHIBIT, [T1 : inf]) ; NET) =
        mte(M, FT, NET) .
  eq mte(M, (L -> T) ; FT, tr(L, PRE, POST, INHIBIT, [T1 : T2]) ; NET) =
        if enabled(M, tr(L, PRE, POST, INHIBIT, [T1 : T2]))
	then min(T2 - T, mte(M, FT, NET))
	else mte(M, FT, NET) fi .

  eq mte(M, empty, NET) = inf .


  --- strange semantics
  op addFiringTimes : Marking FiringTimes Net Time -> FiringTimes .
  eq addFiringTimes(M, (L -> T1) ; FT,
                    tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET,
		            T)
     = (if (PRE <= M) and inhibited(M, INHIBIT)
        then (L -> T1) 
        else (L -> T1 + T) 
       fi) ; 
       addFiringTimes(M, FT, NET, T) .

  eq addFiringTimes(M, empty, NET, T) = empty .

endm
