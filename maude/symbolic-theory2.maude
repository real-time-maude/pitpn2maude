***(
-------------
symbolic-theory2.maude
-------------

An extension of ./symbolic-theory.maude where disabled transitions are 
reset and time does not advance for them. 

*)

load common-sym .

mod PETRI-NET-DYNAMICS is
   protecting PETRI-NET-REPRESENTATION .
   protecting META-LEVEL .

   --- Tick state 
   sort TickState .
   ops tick non-tick : -> TickState [ctor] .

   --- State:
   sort NState . --- Net State
   sort State .
   op _:_:_:_ : TickState Marking FiringTimes Net -> NState [ctor format (n d n d n d n d) prec 75] .

   --- Global state 
   op {_,_,_} : Nat NState BooleanExpr -> State [ctor format(n d n d n d n d)] .

   ---------------------------------------------------
   vars T T1 T2 T3                     : Time .
   var TI                              : TimeInf .
   vars NET NET'                       : Net .
   vars M M' PRE POST INHIBIT          : Marking .
   vars PRE-M INTERMEDIATE-M POST-M    : Marking .
   vars L L'                           : Label .
   vars P P1 P2                        : Place .
   vars FT FT'                         : FiringTimes .
   var INTERVAL                        : Interval .
   vars C C'                           : BooleanExpr .
   vars R1 R2                          : RealExpr .
   vars R1' R2'                        : RealExpr .
   var RT                              : RealExpr .
   vars ST ST'                         : NState .
   var n n'                            : Nat .
   vars N1 N1'                         : IntegerExpr .
   var TICK                            : TickState   .
   ---------------------------------------------------

   crl [applyTransition] :
       { n ,
         TICK : --- no constrains on the flag 
         M : ((L -> R1) ; FT) : (tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET) ,
         C }
     =>
      { n ,
        non-tick : 
        POST-M :
        ((L -> 0/1) ; simplifyFormula(updateFiringTimes(FT, PRE-M , POST-M, NET))) :
        (tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET),
        C' }
    if  PRE-M := M - PRE /\ 
        POST-M := simplifyFormula(PRE-M + POST) /\
        C' := simplifyFormula(C and 
        (PRE <= M) and (R1 in INTERVAL) and
        not inhibited(M, tr(L, PRE, POST, INHIBIT, INTERVAL)))  /\
        smtCheck(C') .

  crl [tick] :
    { n , 
      non-tick : M : FT : NET, 
      C }
     =>
     {   n + 1 ,
         tick : M : simplifyFormula(addFiringTimes(M, FT, NET, R1)) : NET,
         C' 
    }
    if  R1 := vt(n) /\
        C' := simplifyFormula(C and R1 >= 0/1 and mte(R1, M, FT, NET)) /\
        smtCheck(C') .

   op updateFiringTimes : FiringTimes Marking Marking Net -> FiringTimes .
   eq updateFiringTimes(empty, INTERMEDIATE-M, POST-M, NET) = empty .
   eq updateFiringTimes((L -> R1) ; FT, INTERMEDIATE-M, POST-M,
                        tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET) =
                        (L -> 
                            ((((not enabled(INTERMEDIATE-M, tr(L, PRE, POST, INHIBIT, INTERVAL)))
                             or (not  enabled(POST-M, tr(L, PRE, POST, INHIBIT, INTERVAL))))) ?
                             0/1 : --- if
                             R1)) --- else 
                             ;  updateFiringTimes(FT, INTERMEDIATE-M, POST-M, NET) .

  op mte : RealExpr Marking FiringTimes Net -> BooleanExpr .
  eq mte(R1 , M, (L -> T) ; FT, tr(L, PRE, POST, INHIBIT, [T1 : inf]) ; NET) =
        mte(R1, M, FT, NET) .
  eq mte(R1, M, (L -> T) ; FT, tr(L, PRE, POST, INHIBIT, [T1 : T2]) ; NET) =
         ((enabled(M, tr(L, PRE, POST, INHIBIT, [T1 : T2]))) ?
         R1 <= T2 - T : true) and mte(R1, M, FT, NET) .

  eq mte(R1, M, empty, NET) = true .


  --- strange semantics
  op addFiringTimes : Marking FiringTimes Net Time -> FiringTimes .
  eq addFiringTimes(M, (L -> T1) ; FT,
                    tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET,
		    RT)
     = ( L -> ( ( ( (PRE <= M) and inhibited(M, INHIBIT)) 
                     or (not enabled(M, tr(L, PRE, POST, INHIBIT, INTERVAL) ))) --- not enabled
       ? T1 : T1 + RT)) ; 
       addFiringTimes(M, FT, NET, RT)
       .

  eq addFiringTimes(M, empty, NET, T) = empty .

endm
