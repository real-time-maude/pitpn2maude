***(
---------------------
symbolic-folding-tree.maude
---------------------

To be executed with Maude-se (https://maude-se.github.io/z3patch)

Different from ./symbolic-folding, this implementation does not check 
folding for all the previous visited states (as in a graph) but only
in the predecessors states (as in a tree). 

*)


load ./z3-simplification .

mod PETRI-NET-DYNAMICS is
   protecting Z3-SIMPLIFY .
   protecting META-LEVEL .

   --- Tick state 
   sort TickState .
   ops tick non-tick : -> TickState [ctor] .

   --- State:
   sort NState . --- Net State
   sort State .
   op _:_:_:_ : TickState Marking FiringTimes Net -> NState [ctor format (n d n d n d n d) prec 75] .

   --- The first argument is used for generating fresh variables 
   op {_,_,_|_} : Nat NState BooleanExpr BooleanExpr -> State [ctor format(n d n d n d n d n d)] .

   --- Pairs of Marking and constraints (to compute the new state with fresh variables)
   sort PairMarkingBoolean .
   op `[_,_`] : Marking BooleanExpr -> PairMarkingBoolean [ctor] .

   --- Pairs of FTs and constraints (to compute the new state with fresh variables)
   sort PairFTBoolean .
   op `[_,_`] : FiringTimes BooleanExpr -> PairFTBoolean  [ctor] .

   --- A tuple Marking FT for matching procedures 
   sort TupleMFT .
   op <_,_> : Marking FiringTimes -> TupleMFT .

   ---------------------------------------------------
   vars T T1 T2 T3                     : Time .
   var TI                              : TimeInf .
   vars NET NET'                       : Net .
   vars PRE POST INHIBIT               : Marking .
   var  M M' M''                       : Marking .
   vars PRE-M INTERMEDIATE-M POST-M    : Marking .
   var new-M                           : Marking .
   vars L L'                           : Label .
   vars P P1 P2                        : Place .
   vars FT FT' FT''                    : FiringTimes .
   vars new-FT                         : FiringTimes .
   var INTERVAL                        : Interval .
   vars C C' C''                       : BooleanExpr .
   var C-PAR                           : BooleanExpr .
   var C-INI                           : BooleanExpr .
   vars C-FT C-M                       : BooleanExpr .
   var  C-NEXT                         : BooleanExpr .
   vars R1 R2                          : RealExpr .
   vars R1' R2'                        : RealExpr .
   var RT                              : RealExpr .
   vars ST ST'                         : NState .
   vars id n n'                        : Nat .
   vars N1 N1'                         : IntegerExpr .
   var TICK                            : TickState   .
   var PMB                             : PairMarkingBoolean .
   var PFTB                            : PairFTBoolean .
   var TERM                            : Term .
   var Q                               : Qid .
   ---------------------------------------------------

   --- For meta-level purposes 
   op error : -> State .
   --- the intial configuration (after one tick)
  op init : Net Marking -> State . 
  --- Constraint on the parameters 
  op init : Net Marking BooleanExpr  -> State . 
  --- Other constraints 
  op init : Qid Net Marking BooleanExpr BooleanExpr  -> State . 
  eq  init(NET, M) = init('PETRI-NET-DYNAMICS, NET, M,  true, true) .
  eq  init(NET, M, C-PAR) = init('PETRI-NET-DYNAMICS, NET, M, C-PAR, true) .

  --- The first BooleanExpr parameter is the (invariant) condition on parameters
  ceq  init(Q, NET, M, C-PAR , C-INI) = 
      {n, tick : new-M : new-FT : NET , C-PAR | (C and C-M) } 
  if TERM := getTerm(metaRewrite([ Q ], upTerm({1, non-tick : M : zero-clock(NET) : NET, C-PAR | C-INI}), 1)) /\
      { n , tick : M' : new-FT : NET, C-PAR | C } := downTerm(TERM, error) /\
      [ new-M , C-M ] := new-marking(1, M')  --- new marking with fresh variables
      .


   crl [applyTransition] :
       { n ,
         tick :
         M : ((L -> R1) ; FT) : (tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET) ,
         C-PAR | C }
     =>
      { n + 1 ,
        non-tick : 
        M'' : --- Marking only with variables 
        ((L -> vc(n,L)) ; FT'' ) : --- Firing time only with variables 
        (tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET),
        C-PAR | C-NEXT }
    if  PRE-M := M - PRE /\
        POST-M := (PRE-M + POST) /\
        C' := (
          C and  
          (PRE <= M) and (R1 in INTERVAL) and
          not inhibited(M, tr(L, PRE, POST, INHIBIT, INTERVAL))
          )  /\
        smtCheck(C-PAR and C') /\
        [ M'' , C-M ] := new-marking(n, POST-M) /\  --- new marking with fresh variables
        FT' := (updateFiringTimes(FT, PRE-M , POST-M, NET)) /\
        [ FT'' , C-FT ] := new-firing-time(n, FT') /\  --- new FT with fresh variables
        C-NEXT :=   elim-ticks(n,
                  C' and 
                  vc(n,L) === 0/1 and  --- reset for L 
                  C-M and
                  C-FT 
                   )
        .

  crl [tick] :
    { n , 
      non-tick : M : FT : NET, 
      C-PAR | C }
     =>
     {   n + 1 ,
         tick : M'' : FT'' : NET, --- FT only with variables 
         C-PAR | C-NEXT 
    }
    if  R1 := vt(n) /\ --- New value for tick
        C' := (C and R1 >= 0/1 and mte(R1, M, FT, NET)) /\
        smtCheck(C-PAR and C') /\
        FT' := (addFiringTimes(M, FT, NET, R1)) /\
        [ FT'' , C-FT ] := new-firing-time(n, FT') /\  --- new FT with fresh variables
        [ M'' , C-M ] := new-marking(n, M) /\  --- new marking with fresh variables
        C-NEXT :=  elim-ticks(n,
          C' and C-FT and C-M
            ) 
        .


   op updateFiringTimes : FiringTimes Marking Marking Net -> FiringTimes .
   eq updateFiringTimes(empty, INTERMEDIATE-M, POST-M, NET) = empty .
   eq updateFiringTimes((L -> R1) ; FT, INTERMEDIATE-M, POST-M,
                        tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET) =
                        (L -> 
                            ((((not enabled(INTERMEDIATE-M, tr(L, PRE, POST, INHIBIT, INTERVAL)))
                             or (not  enabled(POST-M, tr(L, PRE, POST, INHIBIT, INTERVAL))))) ?
                             0/1 : --- if
                             R1)) --- else 
                             ;  updateFiringTimes(FT, INTERMEDIATE-M, POST-M, NET) .

  op mte : RealExpr Marking FiringTimes Net -> BooleanExpr .
  eq mte(R1 , M, (L -> T) ; FT, tr(L, PRE, POST, INHIBIT, [T1 : inf]) ; NET) =
        mte(R1, M, FT, NET) .
  eq mte(R1, M, (L -> T) ; FT, tr(L, PRE, POST, INHIBIT, [T1 : T2]) ; NET) =
         ((enabled(M, tr(L, PRE, POST, INHIBIT, [T1 : T2]))) ?
         R1 <= T2 - T : true) and mte(R1, M, FT, NET) .

  eq mte(R1, M, empty, NET) = true .


  --- strange semantics
  op addFiringTimes : Marking FiringTimes Net Time -> FiringTimes .
  eq addFiringTimes(M, (L -> T1) ; FT,
                    tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET,
		    RT)
     = ( L -> ( ( ( (PRE <= M) and inhibited(M, INHIBIT)) 
                     or (not enabled(M, tr(L, PRE, POST, INHIBIT, INTERVAL) ))) --- not enabled
       ? T1 : T1 + RT)) ; 
       addFiringTimes(M, FT, NET, RT)
       .

  eq addFiringTimes(M, empty, NET, T) = empty .

  --- Generating a fresh variable for each element of the marking
  op new-marking : Nat Marking -> PairMarkingBoolean .
  op $new-marking : Nat Marking PairMarkingBoolean -> PairMarkingBoolean .
  eq new-marking(n, M) = $new-marking(n, M , [ empty , true ]) .
  eq $new-marking(n, (empty).Marking, PMB) = PMB .
  eq $new-marking(n, ( P |-> N1) ; M , [ M' , C ]) = 
     $new-marking(n , M, [ ( M' ; P |-> vm(n, P)) , (C and vm(n, P) === N1)]) .

  op new-firing-time : Nat FiringTimes -> PairFTBoolean .
  op $new-firing-time : Nat FiringTimes PairFTBoolean -> PairFTBoolean .
  eq new-firing-time(n, FT) = $new-firing-time(n, FT , [ empty , true ]) .
  eq $new-firing-time(n, (empty).FiringTimes, PFTB) = PFTB .
  eq $new-firing-time(n, ( (L -> T1) ; FT) , [ FT' , C ]) = 
     $new-firing-time(n , FT, [ ( FT' ; L -> vc(n, L)) , (C and vc(n, L) === T1)]) .

  --- Generating fresh variables with "now" 
  op marking-now : Marking -> BooleanExpr .
  eq marking-now((empty).Marking) = true .
  eq marking-now(( P |-> N1) ; M ) = 
     vm(now, P) === N1 and marking-now( M ) .

  op FT-now : FiringTimes -> BooleanExpr .
  eq FT-now((empty).FiringTimes) = true .
  eq FT-now(( L -> R1) ; FT ) = 
     vc(now, L) === R1 and FT-now( FT ) .

  op M-FT-now : Marking FiringTimes -> BooleanExpr .
  eq M-FT-now(M,FT) = marking-now(M) and FT-now(FT) .
endm

mod SYMBOLIC-FOLDING is

   --- Note the renaming of the subsort State 
   including  PETRI-NET-DYNAMICS * (sort State to $State, op init to $init) .

   --- The net is not part of NState 
   op _:_:_ : TickState Marking FiringTimes -> NState [ctor format (n d n d n d) prec 75] .

   --- List of $States  {Nat, Nstate, BooleanExpr }
   --- For efficiency, the NState does not include the NET
   sort LState .
   sort N$State .
   op st : Nat $State -> N$State .
   subsort N$State < LState .
   op nil : -> LState .
   op __ : LState LState -> LState [ctor assoc id: nil] .

   --- Constraints of the previous states (identified with the parameter Nat and the Id of the parent)
   sort NConstraint SNConstraint .
   op st : Nat Nat BooleanExpr -> NConstraint .
   subsort NConstraint < SNConstraint . 
   op empty : -> SNConstraint .
   op _;_ : SNConstraint SNConstraint -> SNConstraint [ctor assoc comm id: empty] .


   --- new definition of state
   sort State .
   --- Two different states in order to alternate between two phases
   --- The Qid is the name of the module defining P and T 
   --- The first Nat is needed to identify the already seen states
   op `[_,_,_:_,_`] : Nat Qid Net SNConstraint LState -> State [ctor frozen] . --- the second argument needs to be filtered/subsumed 
   op `{_,_,_:_,_`} : Nat Qid Net SNConstraint LState -> State [ctor frozen] . --- already filtered and ready for the next step

   ---------------------------------------------------
   vars NET NET'                       : Net .
   var  M M' M''                       : Marking .
   var new-M                           : Marking .
   vars L L'                           : Label .
   vars P P1 P2                        : Place .
   vars FT FT' FT''                    : FiringTimes .
   var new-FT                          : FiringTimes .
   vars C C' C''                       : BooleanExpr .
   vars C-FT C-M                       : BooleanExpr .
   var  C-NEXT                         : BooleanExpr .
   vars R1 R1'                         : RealExpr .
   vars ST ST'                         : NState .
   var n n'                            : Nat .
   vars parent parent'                 : Nat . 
   var N-SOL                           : Nat .
   var k                               : IntegerExpr .
   vars N1 N1'                         : IntegerExpr .
   var TICK                            : TickState   .
   var PMB                             : PairMarkingBoolean .
   var PFTB                            : PairFTBoolean .
   vars LS LS' LS''                    : LState .
   vars FRONTIER FRONTIER'             : LState .
   vars ALL-STATES ALL-STATES'         : SNConstraint .
   vars NS NS'                         : $State . 
   var RESULT                          : ResultTriple? .
   var STATE                           : State .
   vars COND COND'                     : StopCondition .
   var S?                              : Substitution? .
   var Str                             : String .
   vars C-FRONT C-ALL                  : BooleanExpr .
   var C-INI C-PAR                     : BooleanExpr .
   var TERM                            : Term .
   var Q                               : Qid .
   ---------------------------------------------------

   --- the intial configuration (after one tick)
  op init : Net Marking -> State . 
  --- Constraint on the parameters 
  op init : Net Marking BooleanExpr  -> State . 
  --- Other constraints 
  op init : Qid Net Marking BooleanExpr BooleanExpr  -> State . 
  eq  init( NET, M ) = init('PETRI-NET-DYNAMICS, NET, M, true, true) .
  eq  init( NET, M,  C-PAR) = init('PETRI-NET-DYNAMICS, NET, M, C-PAR, true) .

  --- The first BooleanExpr parameter is the (invariant) condition on parameters
  ceq  init(Q, NET, M, C-PAR , C-INI) = 
      { 1, Q , NET : empty , st(0, {n, tick : new-M : new-FT, C-PAR | C  }) }
      if { n , tick : new-M : new-FT : NET, C-PAR | C } := $init(Q, NET, M, C-PAR, C-INI)
      .

   --- Two rules alternating between the two states 
   crl [filter] : [ n, Q, NET : ALL-STATES , FRONTIER ] =>
                  { n, Q, NET : ALL-STATES , FRONTIER' }
        if FRONTIER' := filter(FRONTIER, ALL-STATES) .

   crl [step] : { n, Q, NET : ALL-STATES , FRONTIER } =>
                [ n + length(FRONTIER), Q, NET : ( ALL-STATES ; to-set(n, FRONTIER) ) , FRONTIER' ]
        if FRONTIER =/= nil /\
           FRONTIER' := step(n, Q, NET, FRONTIER) . 


    --- Number of elements in a LState 
    op length : LState -> Nat .
    eq length(nil) = 0 .
    eq length(st(parent, NS) FRONTIER) = 1 + length(FRONTIER) .

    --- Transforming a LState into a SNConstraint .
    op to-set : Nat LState -> SNConstraint .
    eq to-set(n, nil) = empty .
    eq to-set(n, st(parent, {n' , TICK : M : FT , C-PAR | C} ) FRONTIER) = 
       st(n, parent, elim-ticks(0,  C and M-FT-now(M, FT))) ; to-set(n + 1, FRONTIER) .


   --- Filtering by subsumption 
   op filter : LState SNConstraint -> LState .
   op filter : LState SNConstraint LState -> LState .
   op $filter : N$State SNConstraint -> LState .
   op subsume : Nat BooleanExpr SNConstraint -> Bool .
   eq filter(LS, ALL-STATES) = filter(LS, ALL-STATES, nil) . --- [print "LS=" LS] .
   eq filter(nil, ALL-STATES, LS') = LS' .
   eq filter( st(parent, NS) FRONTIER, ALL-STATES, LS') = 
      filter(FRONTIER, ALL-STATES, $filter(st(parent, NS), ALL-STATES) LS') .

   eq $filter(st(parent, {n ,tick : M : FT , C-PAR | C}), ALL-STATES) = 
     if subsume(parent, C-PAR and elim-ticks(0, C and M-FT-now(M, FT)), ALL-STATES) 
     then nil
     else st(parent, {n ,tick : M : FT , C-PAR | C})
     fi .

   --- 0 is assumed to be the root of the tree
   eq subsume(0, C, ALL-STATES) = false .
   --- ceq subsume(parent, C, (st(parent, parent', C') ; ALL-STATES)) =
      --- smtCheck(not ( C implies C')) == false
      --- or-else subsume(parent', C, ALL-STATES)
   ceq subsume(parent, C , ALL-STATES) =
      smtCheck(not (C implies b-parents(parent, ALL-STATES))) == false 
      if parent > 0 .

    op b-parents : Nat SNConstraint -> BooleanExpr [memo] .
    eq b-parents(0, ALL-STATES) = false .
    eq b-parents(parent, empty) = false .
    eq b-parents(parent, st(parent, parent', C) ; ALL-STATES) = C or b-parents(parent', ALL-STATES) .


   --- Next states 
   op step : Nat Qid Net LState -> LState .
   op step : Nat Qid Net LState LState -> LState .
   eq step(n, Q, NET, LS) = step(n, Q, NET, LS, nil) .
   eq step(n, Q, NET, nil, LS') = LS' .
   eq step(n, Q, NET, (st(parent,NS) LS), LS') = 
      step(n + 1, Q, NET, LS, successor(n, Q, NET, NS) LS') . 

  --- Successors (from tick states to the next ticked stated)
  op successor : Nat  Qid Net $State -> LState .
  op successor : Nat Qid Net $State Nat -> LState .
  eq successor(parent, Q, NET, { n , tick : M : FT, C-PAR | C }) = 
     successor(parent, Q, NET, { n , tick : M : FT, C-PAR | C }, 0) .

  ceq successor(parent, Q, NET, { n , tick : M : FT, C-PAR | C }, N-SOL) = 
    if RESULT == failure 
    then nil
    else st(parent, to-nstate(getTerm(RESULT))) successor(parent, Q, NET, { n , tick : M : FT, C-PAR | C }, N-SOL + 1)
    fi 
  if RESULT := 
       metaSearch([Q],
                  upTerm({n, tick : M : FT : NET, C-PAR | C}),
                  '`{_`,_`,_|_`}['$N:Nat, '_:_:_:_['tick.TickState, '$M:Marking, '$FT:FiringTimes, '$NET:Net], '$C-PAR:BooleanExpr, '$C:BooleanExpr],
                  nil, 
                  '+,
                  2, --- only one step until the next ticked states
                  N-SOL) .
  --- eliminating the Net from the result
  op to-nstate : Term -> $State .
  --- error should not be produced here (and the owise clause is not needed)
  ceq to-nstate(TERM) = {n, TICK : M : FT, C-PAR | C} 
   if { n, TICK : M : FT : NET , C-PAR | C } := downTerm(TERM, error) .

  --- ---------------------
  --- Conditions for search
  --- ---------------------
  sort StopCondition .
  --- bound(4) stops in the 4th iteration of the algorithm
  --- note that each step includes 2 rewrites: tick + transition, hence the " * 2" below
  op bound : Nat -> StopCondition .
  --- Check if the marking M in reach(M) has been reached. 
  op reach : Marking -> StopCondition .
  op k-bounded : IntegerExpr -> StopCondition .
  op _and_ : StopCondition StopCondition -> StopCondition [ctor assoc] .
  op _or_ : StopCondition StopCondition -> StopCondition [ctor assoc] .
  op not_ : StopCondition -> StopCondition [ctor] .

  op check : State StopCondition -> Bool .
  op check : LState StopCondition -> Bool .
  op $check : $State StopCondition -> Bool .
  eq check( { n, Q, NET : ALL-STATES, FRONTIER }, COND ) = 
    check(FRONTIER, COND ).
  --- only { states } are checked 
  eq check( [ n,  Q, NET : ALL-STATES, FRONTIER ], COND ) = false .

  eq check(nil, COND) = false .
  eq check(st(parent, NS) FRONTIER, COND) = $check(NS, COND) or-else check(FRONTIER, COND) .
  eq $check( { n , tick : M : FT, C-PAR | C } , bound(n')) = n >= 2 * n' .
  eq $check( { n , tick : M : FT, C-PAR | C } , reach(M')) = smtCheck(C and M' <= M) .
  eq $check( { n , tick : M : FT, C-PAR | C } , k-bounded(k)) = smtCheck(C and (M <= k-marking(k,M) )) .
  eq $check( NS, COND and COND') = $check(NS, COND) and $check(NS, COND') .
  eq $check( NS, COND or COND') = $check(NS, COND) or $check(NS, COND') .
  eq $check( NS, not COND ) = not $check(NS, COND) .

  --- k-marking
  op k-marking : IntegerExpr Marking -> Marking .
  eq k-marking(k, empty) = empty .
  eq k-marking(k, P |-> N1 ; M  ) = P |-> k ; k-marking(k, M) .

  --- Find procedure
  --- returns the boolean on the parameters and the final constraint 
  op fail : -> $State .
  op fail : -> State .
  op find : Qid State StopCondition -> PairBoolean .
  ceq find(Q, STATE, COND) =
    if RESULT == failure 
       then < false , false >
    else get-cond-state(downTerm(getTerm(RESULT), fail), COND)
    fi 
  if
    RESULT :=   
       metaSearch([Q],
                  upTerm(STATE), 
                  '$FINAL:State,
                  'check['$FINAL:State, upTerm(COND) ] = 'true.Bool , 
                  '*,
                  unbounded, 
                  0) .
   --- Return the first state in the frontier that satisfies the condition
   op get-cond-state : State StopCondition -> PairBoolean .
   op $get-cond-state : LState StopCondition -> PairBoolean .
   eq get-cond-state(fail, COND) = < false, false >  .
   eq get-cond-state([ n, Q, NET : ALL-STATES , FRONTIER], COND) = 
      < false, false >  .
   eq get-cond-state({ n, Q, NET : ALL-STATES , FRONTIER}, COND) = 
      $get-cond-state(FRONTIER, COND) .
   eq $get-cond-state(nil, COND) = < false , false > .
   eq $get-cond-state( st(n', {n, TICK : M : FT , C-PAR | C  }) LS , COND) = 
     if (check(st(n', {n, TICK : M : FT , C-PAR | C }) , COND)) 
       then <  C-PAR , C >
       else $get-cond-state(LS, COND)
     fi .

endm

