# pitpn2maude

We give a rewriting logic semantics for parametric time Petri nets with inhibitor arcs (PITPNs)

Details about the translation can be found in [this technical report](./paper.pdf).

## Getting started

The project was tested in [Maude 3.2](http://maude.cs.illinois.edu/) and [Maude
SE](https://maude-se.github.io/). A script written in [Python
3.10](https://www.python.org/) is used to parse Roméo input files into Maude
files.

### Maude files

The specification can be found in the folder `maude`.
See the headers of each file for further information.

### Parser

The parser of Roméo models into Maude theories can be found in `pitpn2maude`.
See the file [README](./pitpn2maude/README.md) for further information about its
implementation in Python.

### Benchmarks

See the file [README](./benchmarks/README.md) for further information about
the benchmarks comparing the performance of Imitator and our analyses.
