import os
from collections import defaultdict

from bs4 import BeautifulSoup


def sanitize_label(label: str, prefix: str = "") -> str:
    """Sanitize the labels of elements in the net

    Parameters
    ----------
    label : str
        label
    prefix : str
        prefix to be added to the label

    Returns
    -------
    str
    """
    prefix_ = str(prefix).strip()
    components = (f"{prefix_}_{label}" if len(prefix_) else str(label)).split("_")
    new_label = components[0] + "".join(x[0].upper() + x[1:] for x in components[1:])
    return new_label


class Place:
    """
    A class to represent a place of a Petri net

    Attributes
    ----------
    id : int
        place's identifier
    label : str
        place's label
    initial : int
        initial marking
    """

    def __init__(self, id: int, label: str, initial: int) -> None:
        """
        Constructor of the class

        Parameters
        ----------
        id : int
            place's  identifier
        label : str
            place's label
        initial : int
            initial marking
        """
        self.id = int(id)
        self.label = str(label)
        self.initial = int(initial)

    def __str__(self):
        return f"Place({self.label},{self.initial})"

    def __repr__(self):
        return str(self)


class Transition:
    """
    A class to represent a transition of a Petri net

    Attributes
    ----------
    id : int
        transition's id
    label : str
        transition's label
    lower : str
        interval's lower bound
    upper : str
        interval's upper bound
    """

    def __init__(self, id: int, label: str, lower: str, upper: str) -> None:
        """Constructor of the class

        Parameters
        ----------
        id : int
            transition's id
        label : str
            transition's label
        lower : str
            interval's lower bound
        upper : str
            interval's upper bound
        """
        self.id = int(id)
        self.label = str(label)
        self.lower = str(lower)
        self.upper = str(upper)

    def __str__(self):
        return f"Transition({self.label}, {self.lower}, {self.upper})"

    def __repr__(self):
        return str(self)


class Arc:
    """
    A class to represent an arc of the Petri net

    Attributes
    ----------
    source : int
        source node of the arc
    target : int
        target node of the arc
    weight : int
        arc's weight
    type : str
        type of the arc
    """

    def __init__(
        self, source: int, target: int, weight: int, type: str = "normal"
    ) -> None:
        """
        Constructor of the class

        Parameters
        ----------
        source : int
            source node of the arc
        target : int
            target node of the arc
        weight : int
            arc's weight
        type : str
            type of the arc
        """
        assert (
            type == "normal" or type == "inhibitor"
        ), "type should be normal or inhibitor"

        self.source = int(source)
        self.target = int(target)
        self.weight = int(weight)
        self.type = str(type)

    def __str__(self):
        name = "InhibitorArc" if self.type == "inhibitor" else "Arc"
        return f"{name}({self.source} |-{self.weight}-> {self.target})"

    def __repr__(self):
        return str(self)


class Constraint:
    """Class to represent a constraint

    Attributes
    ----------
    left : str
        left term of the constraint
    op : str
        operator of the constraint
    right : str
        right term of the constraint
    """

    def __init__(self, left, op, right) -> None:
        """Constructor of the class

        Parameters
        ----------
        left : str
            left term of the constraint
        op : str
            operator of the constraint
        right : str
            right term of the constraint
        """
        self.left = left
        self.op = op
        self.right = right

    def __str__(self) -> str:
        return f"Constraint({self.left} {self.op} {self.right})"

    def __repr__(self):
        return str(self)


class Net:
    """
    A class to represent a Petri net

    Attributes
    ----------
    name : str
        name of the Petri net
    places : list[Place]
        list of places of the Petri net
    transitions : list[Transition]
        list of transitions of the Petri net
    """

    def __init__(self, name: str) -> None:
        """
        Constructor of the class

        Parameters
        ----------
        name : str
            name of the Petri net
        """
        self.name = str(name)
        self.constraints = list()
        self._places = defaultdict()
        self._transitions = defaultdict()
        self._ingoing_arcs = defaultdict(list)
        self._outgoing_arcs = defaultdict(list)

    def add_constraint(self, constraint: Constraint) -> None:
        """Add a constraint to the model

        Parameters
        ----------
        constraint : Constraint
            new constraint
        """
        self.constraints.append(constraint)

    def add_place(self, place: Place) -> None:
        """
        Add a new place to the Petri net

        Parameters
        ----------
        place : Place
            the new place
        """
        self._places[place.id] = place

    def add_transition(self, transition: Transition) -> None:
        """
        Add a new transition to the Petri net

        Parameters
        ----------
        transition : Transition
            the new transition
        """
        self._transitions[transition.id] = transition

    def get_place(self, place_id: int) -> Place:
        """
        Get a place by its identifier

        Parameters
        ----------
        place_id : int
            place's identifier

        Returns
        -------
        Place
        """
        return self._places[place_id]

    def get_transition(self, transition_id: int) -> Transition:
        """
        Get a transition by its identifier

        Parameters
        ----------
        transition_id : int
            transition's identifier

        Returns
        -------
        Transition
        """
        return self._transitions[transition_id]

    def add_ingoing_arc(self, arc: Arc) -> None:
        """
        Add an arc from a place to a transition

        Parameters
        ----------
        arc : Arc
            arc to be added
        """
        self._ingoing_arcs[arc.target].append(arc)

    def add_outgoing_arc(self, arc: Arc) -> None:
        """
        Add an arc from a transition to a place

        Parameters
        ----------
        arc : Arc
            arc to be added
        """
        self._outgoing_arcs[arc.source].append(arc)

    def get_arcs_to(self, transtion_id: int) -> list[Arc]:
        """
        Get the arcs pointing to a transition

        Parameters
        ----------
        transtion_id : int
            transition's identifier

        Returns
        -------
        list[Arc]
            list of arcs
        """
        return self._ingoing_arcs[transtion_id]

    def get_arcs_from(self, transition_id: int) -> list[Arc]:
        """
        Get the arcs going from a transition

        Parameters
        ----------
        transition_id : int
            transition's identifier

        Returns
        -------
        list[Arc]
            list of arcs
        """
        return self._outgoing_arcs[transition_id]

    @property
    def places(self) -> list[Place]:
        """
        Get the places of the Petri net

        Returns
        -------
        list[Place]
            list of palces
        """
        return list(self._places.values())

    @property
    def transitions(self) -> list[Transition]:
        """
        Get the transitions of the Petri net

        Returns
        -------
        list[Transition]
            list of transitions
        """
        return list(self._transitions.values())

    @staticmethod
    def from_xml(filename: str):
        """
        Returns a Petri net from a XML file using Romeo's syntax

        Parameters
        ----------
        filename : str
            Path to the Romeo file

        Returns
        -------
        Net
            Petri net model
        """
        with open(filename) as file:
            soup = BeautifulSoup(file, "xml")

            filename = str(soup.find("TPN")["name"])  # type: ignore
            model_name = sanitize_label(os.path.basename(filename).split(".")[0])

            # create new model
            model = Net(model_name)

            # parse initial constraints
            for c in soup.find_all("constraint"):
                # parse operator
                operator = c["op"].strip()
                op_str = ""
                if operator == "Greater":
                    op_str = ">"
                else:
                    raise RuntimeError(
                        "unable to handle constraint operator " + operator
                    )

                # add constraint
                model.add_constraint(
                    Constraint(c["left"].strip(), op_str, c["right"].strip())
                )

            # parse places
            for p in soup.find_all("place"):
                label = sanitize_label(str(p["label"]), "p")
                model.add_place(Place(int(p["id"]), label, int(p["initialMarking"])))

            # parse transitions
            for t in soup.find_all("transition"):
                label = sanitize_label(str(t["label"]), "t")
                lower_bound = t["eft_param"] if t.has_attr("eft_param") else t["eft"]
                upper_bound = t["lft_param"] if t.has_attr("lft_param") else t["lft"]

                model.add_transition(
                    Transition(int(t["id"]), label, lower_bound, upper_bound)
                )

            # parse arcs
            for a in soup.find_all("arc"):
                arc_type = a["type"]
                place = int(a["place"])
                transition = int(a["transition"])
                weight = int(a["weight"])

                if arc_type == "PlaceTransition":
                    model.add_ingoing_arc(Arc(place, transition, weight))
                elif arc_type == "logicalInhibitor":
                    model.add_ingoing_arc(Arc(place, transition, weight, "inhibitor"))
                elif arc_type == "TransitionPlace":
                    model.add_outgoing_arc(Arc(transition, place, weight))
                else:
                    raise RuntimeError("unable to handle arc of type " + arc_type)

            return model

    def __str__(self):
        return f"Net({self.name}, places={self.places}, transitions={self.transitions})"

    def __repr__(self):
        return str(self)
