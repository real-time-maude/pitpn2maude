import re
from enum import Enum
from typing import Optional

# types of the import
ImportType = Enum("ImportType", "PROTECTING EXTENDING INCLUDING")


class Maude:
    """A class encapsulating methods to generate statements in Maude"""

    def _remove_extra_spaces(self, text) -> str:
        """Remove extra spaces in a text"""
        return re.sub(r"\s+", " ", text)

    def module(self, name: str, statements: str, functional: bool) -> str:
        """
        Maude statement for a module

        Parameters
        ----------
        name : str
            name of the module
        statements : str
            statements inside the module
        functional : bool
            Flag to set wther the module is equational or not

        Returns
        ------
        str
        """
        prefix = "fm" if functional else "m"
        module_str = f"{prefix}od {name} is\n\t{statements}\nend{prefix}"

        return module_str

    def loadFile(self, filename: str) -> str:
        """
        Maude statement to load a file

        Parameters
        ----------
        filename : str
            name of the file

        Returns
        ------
        str
        """
        load_str = f"load {filename} ."
        return self._remove_extra_spaces(load_str)

    def importModule(self, name: str, import_type: ImportType) -> str:
        """
        Maude statement to import a module

        Parameters
        ----------
        name : str
            name of the module
        import_type : ImportType
            type of the import

        Returns
        ------
        str
        """
        keyword = import_type.name.lower()
        import_str = f"{keyword} {name} ."

        return self._remove_extra_spaces(import_str)

    def sorts(self, identifiers: list[str]) -> str:
        """
        Maude statement to declare sorts

        Parameters
        ----------
        identifiers : list[str]
            identifier of the sorts

        Returns
        ------
        str
        """
        sorts_l = " ".join(identifiers)
        keyword = "sorts" if (len(identifiers) > 1) else "sort"
        sorts_str = f"{keyword} {sorts_l} ."

        return self._remove_extra_spaces(sorts_str)

    def subsorts(self, left_sorts: list[str], *right_sorts: list[str]) -> str:
        """
        Maude statement to declare subsorts

        Parameters
        ----------
        left_sorts : list[str]
            leftmost subsort
        *right_sorts : list[str]
            right subsort

        Returns
        ------
        str
        """
        keyword = "subsorts" if (len(right_sorts) > 1) else "subsort"

        left_sorts_str = " ".join(left_sorts)
        right_sorts_str = " < ".join(map(lambda l: " ".join(l), right_sorts))
        subsorts_str = f"{keyword} {left_sorts_str} < {right_sorts_str}"

        return self._remove_extra_spaces(subsorts_str)

    def variables(self, identifiers: list[str], sort: str) -> str:
        """
        Maude statement to declare variables

        Parameters
        ----------
        identifiers : list[str]
            identifiers of the variables
        sort : str
            sort of the variables

        Returns
        ------
        str
        """
        keyword = "vars" if (len(identifiers) > 1) else "var"
        identifiers_l = " ".join(identifiers)
        variables_str = f"{keyword} {identifiers_l} : {sort} ."

        return self._remove_extra_spaces(variables_str)

    def statement(self, statement: str, attrs: list[str], conditions: list[str]) -> str:
        """
        Generate a Maude statement.

        Parameters
        ----------
        statement : str
            Maude statement
        attrs : list[str]
            attributes of the statement
        conditions : list[str]
            conditions of the statement

        Returns
        ------
        str
        """
        attrs_l = " ".join(attrs)
        attrs_str = f"[{attrs_l}] " if (len(attrs_l)) else ""

        cond_l = " /\\ ".join(conditions)
        conds_str = f"if {cond_l} " if (conditions) else ""

        statement_str = f"{statement} {conds_str}{attrs_str}."
        return self._remove_extra_spaces(statement_str)

    def operators(
        self,
        identifiers: list[str],
        domain_sorts: list[str],
        range_sort: str,
        attrs: list[str],
    ) -> str:
        """
        Maude statement to declare an operator

        Parameters
        ----------
        identifiers : list[str]
            identifier of the operator
        domain_sorts : list[str]
            sorts of the operator's domain
        range_sort : str
            sort of the operator's range
        attrs : list[str]
            attributes of the operator

        Returns
        ------
        str
        """
        keyword = "ops" if (len(identifiers) > 1) else "op"
        identifiers_l = " ".join(identifiers)
        domain = " ".join(domain_sorts)

        operators_str = f"{keyword} {identifiers_l} : {domain} -> {range_sort}"
        return self.statement(operators_str, attrs, [])

    def membership(
        self, term: str, sort: str, attrs: list[str], conditions: list[str]
    ) -> str:
        """
        Maude statement to declare a membership

        Parameters
        ----------
        term : str
            membership term
        sort : str
            sort of the term
        attrs : list[str]
            attributes of the membership
        conditions : list[str]
            conditions of the membership

        Returns
        ------
        str
        """
        keyword = "cmb" if (len(conditions)) else "mb"
        membership_str = f"{keyword} {term} : {sort}"
        return self.statement(membership_str, attrs, conditions)

    def equation(
        self, left_term: str, right_term: str, attrs: list[str], conditions: list[str]
    ) -> str:
        """
        Maude statement to declare an equation

        Parameters
        ----------
        left_term : str
            left side of the equation
        right_term : str
            right side of the equation
        attrs : list[str]
            attributes of the equation
        conditions : list[str]
            conditions of the equation

        Returns
        ------
        str
        """
        keyword = "ceq" if (len(conditions)) else "eq"
        equation_str = f"{keyword} {left_term} = {right_term}"
        return self.statement(equation_str, attrs, conditions)

    def rule(
        self,
        label: Optional[str],
        left_term: str,
        right_term: str,
        attrs: list[str],
        conditions: list[str],
    ) -> str:
        """
        Maude statement to declare a rule

        Parameters
        ----------
        label : str or None
            label of the rule
        left_term : str
            left side of the rule
        right_term : str
            right side of the rule
        attrs : list[str]
            attributes of the rule
        conditions : list[str]
            conditions of the rule

        Returns
        ------
        str
        """
        keyword = "crl" if (len(conditions)) else "rl"
        label_str = f" [{label}] :" if label is not None else ""
        rule_str = f"{keyword}{label_str} {left_term} => {right_term}"
        return self.statement(rule_str, attrs, conditions)
