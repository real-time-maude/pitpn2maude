import re
import textwrap
from abc import ABC, abstractmethod
from typing import Union

from src.Maude import ImportType, Maude
from src.Net import Net, Transition


class Parser(ABC):
    """
    A class to represent the parser of Petri Nets to Maude

    Attributes
    ----------
    maude : Maude
        Object containing methods to generate Maude statements
    """

    def __init__(self, theory) -> None:
        """
        Constructor of the class
        """
        self.theory = theory
        self.eq_name = "petri-net"
        self.maude = Maude()

    @abstractmethod
    def to_maude(self, model: Net) -> str:
        """
        Parse a Petri net model into Maude

        Parameters
        ----------
        model : Net
            Petri net model
        theory_file : str
            rewriting theory to be loaded

        Returns
        -------
        str
        """
        pass

    def _get_place_sort(self, model: Net, place: Union[int, str], marking: int) -> str:
        """
        Returns the Maude representation of a place

        Parameters
        ----------
        model : Net
            Petri net model
        place : int, str
            identifier of the place
        marking : int
            marking of the place

        Returns
        -------
        str
        """
        place_label = model.get_place(place).label if isinstance(place, int) else place
        return f"{place_label} |-> {marking}"

    def _get_transition_sort(self, model: Net, transition: Transition) -> str:
        """
        Return the Maude representation of a transition

        Parameters
        ----------
        model : Net
            Petri net model
        transition : Transition
            Petri net transition

        Returns
        -------
        str
        """
        incoming_arcs = model.get_arcs_to(transition.id)
        # get the normal incomming arcs of the transition
        pre = [
            f"{self._get_place_sort(model, a.source, a.weight)}"
            for a in incoming_arcs
            if a.type == "normal"
        ]
        pre_str = " ; ".join(pre) if len(pre) else "empty"

        # get the outgoing arcs of the transition
        post = [
            f"{self._get_place_sort(model, a.target, a.weight)}"
            for a in model.get_arcs_from(transition.id)
        ]
        post_str = " ; ".join(post) if len(post) else "empty"

        # get the inhibitor arcs to the transition
        inhibitor = [
            f"{self._get_place_sort(model, a.source, a.weight)}"
            for a in incoming_arcs
            if a.type == "inhibitor"
        ]
        inhibitor_str = " ; ".join(inhibitor) if len(inhibitor) else "empty"

        lower_bound = self._get_parameter_str(transition.lower)

        upper_bound = (
            self._get_parameter_str(transition.upper)
            if transition.upper != "inf"
            else "inf"
        )

        t_str = f"tr({transition.label}, {pre_str}, {post_str}, {inhibitor_str}, [{lower_bound} : {upper_bound}])"

        return t_str

    def _get_net_equation(self, model: Net) -> str:
        """
        Return the petri net equations in Maude

        Parameters
        ----------
        model : Net
            Petri net model

        Returns
        -------
        str
        """
        net_str = self.maude.operators([self.eq_name], [], "Net", [])

        # petri net equation
        net_eq_term = [self._get_transition_sort(model, t) for t in model.transitions]
        net_eq_str = self.maude.equation(self.eq_name, " ; ".join(net_eq_term), [], [])
        net_eq_str_pretty = ") ;\n                           ".join(
            net_eq_str.split(") ; ")
        )

        eq = f"""
            {net_str}
            {net_eq_str_pretty}"""

        return eq

    def parse_file(self, filename: str) -> str:
        """
        Parse a Romeo file into a Maudel model

        Parameters
        ----------
        filename : str
            Romeo file

        Returns
        -------
        str
        """
        model = Net.from_xml(filename)
        output = self.to_maude(model)

        return output

    def _is_parameter(self, value: str) -> bool:
        """Check if the value is a parameter

        Parameters
        ----------
        value : str
            value to be checked

        Returns
        -------
        bool
        """
        ops = ["*", "+", "-", "/", "inf"]
        not_expr = len([e for e in ops if e in value]) == 0
        return not_expr and re.match(r"^-?\d+(?:\.\d+)?$", value) is None

    def _get_parameter_str(self, value: str) -> bool:
        """Return the well formed string of a parameter or value"""
        match_param = re.search(r"[a-zA-Z]+\d*", value)
        match_number = re.search(r"\d+\.?\d*", value)
        match_op = re.search(r"\*|-|\+|/", value)
        param = f'vp("{match_param.group(0)}")' if match_param is not None else ""
        number = f"{match_number.group(0)}/1" if match_number is not None else ""
        op = f"{match_op.group(0)}" if match_op is not None else ""

        # it's a expression
        if op == "":
            return param + number
        else:
            return (
                f"{param} {op} {number}"
                if match_param.start() == 0
                else f"{number} {op} {param}"
            )


class SymbolicTheory(Parser):
    def __init__(self, theory: str) -> None:
        super().__init__(theory)

    def _get_preamble(self, model: Net) -> str:
        """
        Get the preamble of the Maude file

        Parameters
        ----------
        model : Net
            Petri net model

        Returns
        -------
        str
        """
        # transition ops
        transitions = [t.label for t in model.transitions]
        transitions_str = self.maude.operators(transitions, [], "Label", [])

        # place ops
        places = [p.label for p in model.places]
        places_str = self.maude.operators(places, [], "Place", [])

        preamble = f"""
            {transitions_str}
            {places_str}"""

        return preamble

    def _get_init_equation(self, model: Net) -> str:
        """Returns the initial state of the Petri net in Maude

        Parameters
        ----------
        model : Net
            Petri net

        Returns
        -------
        str
        """
        eq_name = "init"
        op_str = self.maude.operators([eq_name], [], "State", [])

        # initial marking
        places = [self._get_place_sort(model, p.label, p.initial) for p in model.places]
        places_str = " ; ".join(places)

        # initial clocks
        transitions = [f"{t.label} -> 0/1" for t in model.transitions]
        transitions_str = " ; ".join(transitions)

        # constraints on parameters
        t_intervals = [(t.lower, t.upper) for t in model.transitions]
        parameters = filter(self._is_parameter, set(sum(t_intervals, ())))
        parameters_init = [f"{self._get_parameter_str(p)} >= 0/1" for p in parameters]
        parameters_init += [
            f"{self._get_parameter_str(lower)} <= {self._get_parameter_str(upper)}"
            for (lower, upper) in t_intervals
            if self._is_parameter(lower)
            and self._is_parameter(upper)
            and lower != upper
        ]
        parameters_init += [
            f"{self._get_parameter_str(c.left)} {c.op} {self._get_parameter_str(c.right)}"
            for c in model.constraints
        ]
        parameters_str = (
            " and ".join(parameters_init) if len(parameters_init) else "true"
        )

        # initial equation
        init_sort = f"{{ 0 , non-tick : {places_str} : {transitions_str} : {self.eq_name} , {parameters_str} }}"
        net_eq_str = self.maude.equation("init", init_sort, [], [])
        net_eq_str_pretty = ":\n                        ".join(net_eq_str.split(": "))

        eq = f"""
            {op_str}
            {net_eq_str_pretty}"""

        return eq

    def _get_search_cmd(self) -> str:
        nb_traces = 1
        nb_steps_str = "*"

        state = "{ n:Nat , TS:TickState : <replace_p> ; REST:Marking : FT:FiringTimes : NET:Net , C:BooleanExpr }"
        condition = (
            "{SS:SatAssignmentSet} := smtCheck(C:BooleanExpr and <replace_m>, true)"
        )
        cmd = f"""search [{nb_traces}] init =>{nb_steps_str}
                {state}
                  such that {condition} ."""
        return cmd

    def to_maude(self, model: Net) -> str:
        """
        Parse a Petri net model into Maude

        Parameters
        ----------
        model : Net
            Petri net model
        theory_file : str
            rewriting theory to be loaded

        Returns
        -------
        str
        """
        # load rewriting theory
        load_theory = self.maude.loadFile(f"./{self.theory}")

        # include petri net dynamics
        import_pn = self.maude.importModule("PETRI-NET-DYNAMICS", ImportType.INCLUDING)

        # maude preamble
        preamble = self._get_preamble(model)

        # petri net equations
        net_eq = self._get_net_equation(model)

        # initial marking equations
        init_eq = self._get_init_equation(model)

        # search command
        search_cmd = self._get_search_cmd()

        model_str = f"""\
        {load_theory}

        mod MODEL is
            {import_pn}
            {preamble}
            {net_eq}
            {init_eq}
        endm

        {search_cmd}

        quit .

        eof"""

        return textwrap.dedent(model_str).strip()


class SymbolicFoldingTheory(Parser):
    def __init__(self, theory: str) -> None:
        super().__init__(theory)

    def _get_init_equation(self, model) -> str:
        """Returns the initial state of the Petri net in Maude

        Returns
        -------
        str
        """
        eq_name = "init"
        eq_mark_name = "init-mark"
        eq_const_name = "init-cons"

        op_str = self.maude.operators([eq_name], [], "State", [])
        op_mark_str = self.maude.operators([eq_mark_name], [], "Marking", [])
        op_const_str = self.maude.operators([eq_const_name], [], "BooleanExpr", [])

        # initial equation
        init_eq_params = ", ".join([self.eq_name, eq_mark_name, eq_const_name])
        init_eq_str = self.maude.equation(
            eq_name, f"{eq_name}({init_eq_params})", [], []
        )

        # initial marking
        places = [
            self._get_place_sort(model, f'"{p.label}"', p.initial) for p in model.places
        ]
        places_str = " ; ".join(places)
        init_mark_str = self.maude.equation(eq_mark_name, places_str, [], [])

        # constraints on parameters
        t_intervals = [(t.lower, t.upper) for t in model.transitions]
        parameters = filter(self._is_parameter, set(sum(t_intervals, ())))
        parameters_init = [f"{self._get_parameter_str(p)} >= 0/1" for p in parameters]
        parameters_init += [
            f"{self._get_parameter_str(lower)} <= {self._get_parameter_str(upper)}"
            for (lower, upper) in t_intervals
            if self._is_parameter(lower)
            and self._is_parameter(upper)
            and lower != upper
        ]
        parameters_init += [
            f"{self._get_parameter_str(c.left)} {c.op} {self._get_parameter_str(c.right)}"
            for c in model.constraints
        ]
        parameters_str = (
            " and ".join(parameters_init) if len(parameters_init) else "true"
        )
        const_mark_str = self.maude.equation(eq_const_name, parameters_str, [], [])

        eq = f"""
            {op_str}
            {init_eq_str}

            {op_mark_str}
            {init_mark_str}

            {op_const_str}
            {const_mark_str}"""

        return eq

    def to_maude(self, model: Net) -> str:
        """
        Parse a Petri net model into Maude

        Parameters
        ----------
        model : Net
            Petri net model
        theory_file : str
            rewriting theory to be loaded

        Returns
        -------
        str
        """
        # load rewriting theory
        load_theory = self.maude.loadFile(f"./{self.theory}")

        # include petri net dynamics
        import_pn = self.maude.importModule("SYMBOLIC-FOLDING", ImportType.PROTECTING)

        # petri net equations
        net_eq = self._get_net_equation(model)

        # initial marking equations
        init_eq = self._get_init_equation(model)

        # search command
        search_cmd = ""
        # search_cmd = self._get_search_cmd(model)

        model_str = f"""\
        {load_theory}

        mod MODEL is
            {import_pn}
            {net_eq}
            {init_eq}
        endm

        {search_cmd}

        quit .

        eof"""

        return textwrap.dedent(model_str).strip()
