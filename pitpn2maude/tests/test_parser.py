import os

import pytest
from src.Parser import SymbolicTheory


def remove_spaces(output_str):
    """Removes all the spaces in a string"""
    output = list(filter(None, [e.strip() for e in output_str.split("\n")]))
    return output


@pytest.mark.parametrize(
    "romeo_file,maude_file,theory",
    [
        ("tutorial.xml", "tutorial.sym-theory.maude", "symbolic-theory"),
        ("tutorial.xml", "tutorial.sym-theory2.maude", "symbolic-theory2"),
        ("scheduling.xml", "scheduling.sym-theory.maude", "symbolic-theory"),
    ],
)
def test_parser(examples_dir, romeo_file, maude_file, theory):
    # select the correct parser
    parser = None
    if theory == "symbolic-theory" or "symbolic-theory2":
        parser = SymbolicTheory(theory)
    else:
        raise "Theory not supported"

    # parse the romeo file
    input_file = os.path.join(examples_dir, romeo_file)
    output = parser.parse_file(input_file)

    # read the file with the expected output
    expected_file = os.path.join(examples_dir, maude_file)
    expected = open(expected_file).read()

    assert remove_spaces(output) == remove_spaces(expected)
