import argparse

from src.Parser import SymbolicFoldingTheory, SymbolicTheory


def main(input_file: str, output_file: str, theory: str):
    try:
        # parse Romeo file into Maude
        parser = None

        if theory == "symbolic-theory" or theory == "symbolic-theory2":
            parser = SymbolicTheory(theory)
        elif theory == "symbolic-folding-tree":
            parser = SymbolicFoldingTheory(theory)
        else:
            raise RuntimeError(f"Theory {theory} is not supported")

        output = parser.parse_file(input_file)

        # write to file
        with open(output_file, "w") as f:
            f.write(output)
            print(f"Output file: {output_file}")
    except Exception as e:
        print(f"Error processing {input_file}.\n {e}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""From Romeo to Maude specifications""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--output", type=str, default="./model.maude", help="Maude output file"
    )

    parser.add_argument(
        "--theory",
        type=str,
        required=True,
        help="Rewriting theory to use",
    )

    parser.add_argument("--input", type=str, required=True, help="Romeo file")

    args = parser.parse_args()
    main(args.input, args.output, args.theory)
