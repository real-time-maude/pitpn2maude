# PITPN2Maude

This repository contains a rewriting logic specification for the verification of
Parametric Interval Timed Petri Nets (PITPN) models.

## Dependencies

The tool was written in [`Python3`](https://www.python.org/downloads/) and the
dependencies can be installed using the following command.

```
pip3 install -r requirements.txt
```

## Getting Started

It takes as input a [`Romeo`](http://romeo.rts-software.org/) model and returns
an [`Maude`](http://maude.cs.illinois.edu/w/index.php/The_Maude_System)
specification.

```
❯ python3 app.py --help
usage: app.py [-h] [--output OUTPUT] --theory THEORY --input INPUT

From Romeo to Maude specifications

options:
  -h, --help       show this help message and exit
  --output OUTPUT  Maude output file (default: ./model.maude)
  --theory THEORY  Rewriting theory to use
  --input INPUT    Romeo file
```

Next, an example of use:

```
python3 app.py --theory symbolic-theory --input tests/examples/tutorial.xml
```

## Running Test

A set of tests can be run with the following command:

```
python3 -m pytest -vv
```
