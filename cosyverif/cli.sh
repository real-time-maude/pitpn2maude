#!/usr/bin/env bash
###################################################################
#         FILE: cli.sh
#        USAGE: ./cli.sh -h
#  DESCRIPTION: Script to configure the cosyverif package
#       AUTHOR: Jaime Arias <arias@lipn.univ-paris13.fr>
###################################################################
set -e

# maude binary
declare -A maude_solvers=(
  ['yices2']="./maude-se-yices2/maude-se-yices2"
  ['z3']="./maude-se-z3/maude-se-z3"
)

# maude theories
theories=("symbolic-theory" "symbolic-theory2" "symbolic-folding-tree")

# Full path to root project
BASE_DIR="$(dirname "$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)")"

##############################################################################
# Copy maude and parser dependencies to the folder
# Arguments:
#   None
##############################################################################
function copy_dependencies {
  # maude theory files
  local maude_theories="${BASE_DIR}/maude"

  # download maude-se-yices2
  if [ ! -d maude-se-yices2 ]; then
    echo -e "\ndownloading Maude-SE with yices ..."
    wget -qO- https://www.dropbox.com/s/u6i9nt0dkmcwd1f/maude-se-yices2.tar.gz | tar xvz
    chmod +x maude-se-yices2/maude-se-yices2
    cp ${maude_theories}/*.maude maude-se-yices2
  fi

  # download maude-se-z3
  if [ ! -d maude-se-z3 ]; then
    echo -e "\ndownloading Maude-SE with z3 ..."
    wget -qO- https://www.dropbox.com/s/oojplavo3ssmdzj/maude-se-z3.tar.gz | tar xvz
    chmod +x maude-se-z3/maude-se-z3
    cp ${maude_theories}/*.maude maude-se-z3
  fi

  # copy romeo parser
  local parser_folder="${BASE_DIR}/pitpn2maude"
  if [ ! -d pitpn2maude ]; then
    echo -e "\ncopying parser ..."
    rsync -av --exclude=".*" ${parser_folder} .
  fi
}

##############################################################################
# Parse romeo model to Maude encoding
# Arguments:
#   Path to the Romeo model (e.g. ${PWD}/benchmarks/models/example.xml)
#   Maude theory (e.g. symbolic-folding-tree)
##############################################################################
function parse_romeo {
  local romeo_model=$1
  local theory=$2
  local parser="./pitpn2maude/app.py"
  local output_file="${romeo_model%.*}.maude"
  echo -e "\nParsing ${romeo_model} with theory \"${theory}\" ..."
  python3 ${parser} --input "${romeo_model}" --output "${output_file}" --theory "${theory}"
}

##############################################################################
# Run Maude
# Arguments:
#   Path to the Maude model (e.g. ${PWD}/benchmarks/models/example.maude)
#   Maude solver (e.g. yices2)
#   Timeout (e.g. 1m)
##############################################################################
function run_maude {
  local file_path=$1
  local solver=$2
  local timeout=$3
  local maude_fullpath=${maude_solvers[$solver]}

  # helpful variables
  local maude_cmd=$(basename "${maude_fullpath}")
  local maude_folder=$(dirname "${maude_fullpath}")
  local filename=$(basename "${file_path}")

  cp "$file_path" $maude_folder
  cd $maude_folder
  echo -e "\nRunning Maude [timeout: ${timeout}] with solver \"${solver}\" and input \"${file_path}\" ..."
  timeout "$timeout" "./${maude_cmd}" -no-banner -batch "${filename}" || echo "timeout ${timeout}"
}

##############################################################################
# Run Maude after parsing the Romeo file
# Arguments:
#   Path to the Romeo model (e.g. ${PWD}/benchmarks/models/example.xml)
#   Maude theory (e.g. symbolic-folding-tree)
#   Maude solver (e.g. yices2)
#   Timeout (e.g. 1m)
##############################################################################
function run_pipeline {
  local romeo_model=$1
  local theory=$2
  local solver=$3
  local timeout=$4
  local maude_file="${romeo_model%.*}.maude"

  parse_romeo "${romeo_model}" ${theory}
  sed -i '/endm/q' "${maude_file}"
  echo "quit ." >>"${maude_file}"
  run_maude "${maude_file}" ${solver} ${timeout}
}

##############################################################################
# Create a package for CosyVerif containing all the dependencies and scripts
# Arguments:
#   None
##############################################################################
function create_package {
  local version=$(git tag --sort=taggerdate | tail -1)
  copy_dependencies

  echo -e "\nBuilding package ..."
  tar -czvf "pitpn2maude-${version}".tar.gz maude-se-yices2 maude-se-z3 pitpn2maude cli.sh

  read -p "Do you want to update the package on the server (Y/n)? " reply
  if [ "$reply" == "Y" ] || [ "$reply" == "y" ]; then
    echo -e "\nUpdating package on the server ..."
    scp pitpn2maude-v1.0.0.tar.gz cosyverif-web:/var/www/html/wp-content/services/Maude/linux/
    rm pitpn2maude-v1.0.0.tar.gz
  fi
}

##############################################################################
# Print the usage message
# Arguments:
#   None
##############################################################################
function usage {
  # Display help
  echo "Usage: $(basename "$0") [-d|h] [-r MODEL THEORY SOLVER TIMEOUT]"
  echo
  echo -e "   -d, --deploy     Create package with all the files."
  echo -e "   -h, --help       Print this help."
  echo -e "   -r, --run        Run Maude using a Petri net as input."
  echo
  exit 1
}

##############################################################################
# Main program
##############################################################################
# If no arguments provided, display usage information
[[ $# -eq 0 ]] && usage

while getopts ":hdr:" options; do
  case "${options}" in
  h) # display Help
    usage ;;
  d) # create the package
    create_package
    exit
    ;;
  r) # run the pipeline
    shift 1
    [[ $# -ne 4 ]] && usage
    run_pipeline "$@"
    exit
    ;;
  *) # incorrect option
    echo -e "Error: Invalid option\n"
    usage
    ;;
  esac
done
